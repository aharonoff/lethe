$(document).ready(function(){
  $(".btn").click(function(){
    $(".popup").remove();
  });
});

var dt = new Date();
dt = addMinutesToDate(dt, 13);
document.getElementById("datetime").innerHTML = dt.toLocaleTimeString([], {
  hour: '2-digit',
  minute: '2-digit'
});

function addMinutesToDate(date, minutes) {
  return new Date(date.getTime() + minutes * 60000);
};
